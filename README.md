#### 专业技能知识体系说明

知识平台星罗棋布，各有各的稳定受众，唯一遗憾的便是，没有一个平台能够做到天下知识汇聚于此。每个人对于知识的喜好不同，倾向性不同。码云作为程序代码的存储仓库，开发者可以不用CSDN，可以不用bilibili，但是，不能不用码云，尤其处于中国环境之下。故而，码云可以作为最好的知识汇聚中心，这就是这个仓库诞生的目的。

#### 软件架构

- [x] 软件架构说明
- [ ] 测试复选框

<table border="0">
  <tr>
    <th>Month</th>
    <th>Savings</th>
    <th>Month</th>
    <th>Savings</th>
    <th>Month</th>
    <th>Savings</th>
    <th>Month</th>
  </tr>
  <tr>
    <td><input type="checkbox" checked="checked" disabled="disabled"/>January</td>
    <td><input type="checkbox" disabled="disabled"/>$100</td>
    <td>January</td>
    <td>$100</td>
    <td>January</td>
    <td>$100</td>
    <td>January</td>
  </tr>
    <tr>
    <th>Month</th>
    <th>Savings</th>
    <th>Month</th>
    <th>Savings</th>
    <th>Month</th>
    <th>Savings</th>
    <th>Month</th>
  </tr>
  <tr>
    <td>January</td>
    <td>$100</td>
    <td>January</td>
    <td>$100</td>
    <td>January</td>
    <td>$100</td>
    <td>January</td>
  </tr>
</table>

#### 安装教程

1. xxxx
2. xxxx
3. xxxx

#### 使用说明

1. xxxx
2. xxxx
3. xxxx

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request

#### 特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5. Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)

| Mon  | Tue  |
|---|---|
| - [x] 软件架构说明
- [ ] 测试复选框  |   |


